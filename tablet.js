const express = require('express');
const mysql = require('mysql2');
const app = express();

const connection = mysql.createConnection({
    host: '172.17.0.2',
    user: 'root',
    password: 'root',
    database: 'punedb'
})

app.use(express.json());

app.get("/tablet", (req,res)=>{
    let query = `select * from tablet`;
    connection.query(query, (error, result)=>{
         if(error != null){
            res.send(error);
            console.log(error);
            res.end();
        }
        else{
            res.send(JSON.stringify(result));
            res.end();
        }
    })
})

app.put("/tablet/:no", (req, res)=>{
    let query = `update tablet set model ='${req.body.model}', price=${req.body.price} where id =${req.params.no};`;
    connection.query(query, (error, result)=>{
        if(error != null){
            res.send("Internal ERORR");
            console.log(error);
            res.end();
        }
        else{
            res.send(result);
            res.end();
        }
    })
})

app.listen(9000)